import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
import { UserInfoModel } from '../models/userInfo';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.scss']
})
export class UsuarioListComponent implements OnInit {
  emps;
  usuarios:UserInfoModel[];
  emp: any;
  form;
  valor: any;
  filterPost = '';

  constructor(private empService: UsuarioService) { }

  ngOnInit() {
    this.emps = this.empService.getEmployees();
  }

  deleteEmployee(id) {
    for(let i = 0; i < this.emps.length; i++) {
      if(this.emps[i].id == id) {
          this.emps.splice(i, 1);
      }
    }

    this.empService.deleteEmployee(id);
  }

  edit(id, nombre, apellido, edad) {

     let form={
      id:id,
      nombre:nombre,
      apellido:apellido,
      edad:edad
    }
    this.empService.updateOld(form);
  }

}
