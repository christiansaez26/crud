import { Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-usuario-create',
  templateUrl: './usuario-create.component.html',
  styleUrls: ['./usuario-create.component.scss']
})
export class UsuarioCreateComponent implements OnInit {

    emps;
    text;
    oldText
    appState = 'default';

    form={
      nombre:'',
      apellido:'',
      edad:''
    }
    emp: any;

    constructor(private empService: UsuarioService) { }

    ngOnInit() {
      this.emps = this.empService.getEmployees();
    }
    
    accion() {
      if (this.form) {
        let newEmployee = {
          id:uuid(),
          nombre: this.form.nombre,
          apellido: this.form.apellido,
          edad: this.form.edad,
        }
        this.emps.push(newEmployee);
        this.empService.addEmployee(newEmployee);
      }
      this.limpiar();
    }

    limpiar() {
      this.form.nombre = '';
      this.form.apellido = '';
      this.form.edad = '';
    }
  }
