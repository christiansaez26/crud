export class UserInfoModel
{
	id: number;
	
	nombre: string;
	apellido: string;

	edad: number;


	constructor(obj: any = null)
	{
		if(obj != null)
		{
			Object.assign(this, obj);
		}
	}
}