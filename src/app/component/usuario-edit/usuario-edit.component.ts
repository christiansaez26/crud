import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../../../services/usuario.service';


@Component({
  selector: 'app-usuario-edit',
  templateUrl: './usuario-edit.component.html',
  styleUrls: ['./usuario-edit.component.scss']
})
export class UsuarioEditComponent implements OnInit {
  emp;
  id;
  
  form={
    nombre:'',
    apellido:'',
    edad:''
  }


  constructor(private _Activatedroute:ActivatedRoute,private empService: UsuarioService) { }

  ngOnInit() {
    
  }

  edit() {
    if (this.form) {
      console.log(this.form)
    this.empService.updateEmployee(this.form);
    }
  }
}
