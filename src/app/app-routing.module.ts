import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioEditComponent } from './component/usuario-edit/usuario-edit.component'


const routes: Routes = [
  {
    path: 'usuario-edit/:id',
    component: UsuarioEditComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
