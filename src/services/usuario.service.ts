import { Injectable } from '@angular/core';
import { Init } from './usuario-init';
import { UserInfoModel } from 'src/app/component/models/userInfo';
import { ActivatedRoute } from '@angular/router';

@Injectable({
    providedIn: 'root',
  })
export class UsuarioService extends Init {
  empss:UserInfoModel[];
  usuarios;
  id: any;
  emps: any;
  
  constructor(private _Activatedroute:ActivatedRoute) {
    super();
    console.log('EmployeeService Works');
    this.load();
   }

   getEmployees() {
     this.usuarios;
     let emp = JSON.parse(localStorage.getItem('employees'));
     this.usuarios=emp;
     return emp;
    }

   addEmployee(newEmp) {
      this.usuarios;
      let emps = JSON.parse(localStorage.getItem('employees'));
      emps.push(newEmp);
      localStorage.setItem('employees', JSON.stringify(emps));
      this.usuarios=JSON.parse(localStorage.getItem('employees'));
    }

   deleteEmployee(id) {
      this.usuarios;
      let emps = JSON.parse(localStorage.getItem('employees'));
      for(let i = 0; i <emps.length; i++) {
        if(emps[i].id == id) {
          emps.splice(i, 1);
        }
      }
      localStorage.setItem('employees', JSON.stringify(emps));
      this.usuarios=JSON.parse(localStorage.getItem('employees'));
    }

   updateOld(old){

    localStorage.setItem('valor',JSON.stringify(old));

   }


     updateEmployee(newEmp){  
      
      this.usuarios;
      var newData = JSON.parse(localStorage.getItem('valor'))
      let emps = JSON.parse(localStorage.getItem('employees'));

      if(newData!=null && newEmp!=null){
        for(let i = 0; i <emps.length; i++) {
          if(emps[i].id == newData.id) {
            emps[i] = newEmp;
          }
        }
        localStorage.setItem('employees', JSON.stringify(emps));
        this.usuarios=JSON.parse(localStorage.getItem('employees'));

    }
   }




}